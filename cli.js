#!/usr/bin/env node

"use strict";

/**
 * CLI script
 */

let convert = require('./'),
    colors = require ('colors'),
    ArgParser = require('argparse').ArgumentParser,
    path = require('path');

// Argparser
let parser = new ArgParser({
    version: '0.0.2',
    addHelp: true,
    prog: 'mdnotes'
});

// Set argparser args
parser.addArgument('file', { 
    help: 'File to convert',
    metavar: 'file',
    nargs: '+'
});
parser.addArgument(['-o', '--outdir'], { 
    help: 'Output directory',
    metavar: 'path',
    defaultValue: ''
});
parser.addArgument(['-s', '--stylesheet'], { 
    help: 'Custom CSS stylesheet',
    metavar: 'path',
    defaultValue: null
});
parser.addArgument(['--header'], { 
    help: 'File with custom header and footer',
    metavar: 'path',
    defaultValue: null
});

// Parse args
let args = parser.parseArgs();
let files = args.file,
    out = args.outdir,
    css = args.stylesheet,
    hdr = args.header;

// Run
console.log('Converting files (%s)', files.length);
files.map(file => {
    convert(file, {
            out: out,
            css: css,
            hdr: hdr
        }, (err, res) => {
        if(err) console.log('✗ '.red + err);
        else console.log('✓ '.green + res);
    });
});